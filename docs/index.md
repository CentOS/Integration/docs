# Introduction

Integration is verifying that products and services built on top of
RHEL or CentOS Stream will continue to work on CentOS Stream and the
next release of RHEL and will not break on package updates.

As RHEL content becomes available only after the release, RHEL-based
services traditionally use a _catching-up integration_ pattern: people
have to adjust their products and services to work on new RHEL after
the update is shipped. Adjusting the services takes time, eating into
the supported RHEL lifecycle period. It also reduces the options for
how we can deal with breaking changes.

CentOS Stream provides a way to enable _forward-looking integration_:
you can do the integration early during the development before the
change is shipped to the CentOS Stream or RHEL repositories. This
allows us to prevent or at least prepare better for any breaking
changes, which might be shipped via CentOS Stream or RHEL updates.

# Purpose of the SIG

Provide a shared space to develop and maintain tooling and knowledge
base on collaborative gating and testing of CentOS Stream updates
before they are published to CentOS mirrors. This includes both -
package-level and compose-level integration.

## Goals

* Document existing integration workflows used by other SIGs.
* Identify common issues.
* Manage, develop and promote the Third-Party CI for CentOS Stream.
   - Document events and triggers.
   - Document standard pipelines.
* Develop the Integration toolkit.

## Deliverables

* CentOS Stream Integration Guide

	The main focus of the SIG at least at the beginning would be to
collect and maintain the knowledge base for integration patterns and
tools used by other SIGs and community members.

* CentOS Third-Party CI service

	Eventually SIG may take the ownership of the third-party CI
service (for example based on CentOS Stream Zuul CI) - this is to be
discussed further.

## First work items

* Develop and document solution to the following question:

	“I have a test case/task I want to run on every change in CentOS
and get notified about the failure. How do I do it?”

* Interview other SIGs for their integration workflows to identify
common patterns, solutions and issues.

# How to join

* Join our Matrix channel:

	<https://chat.fedoraproject.org/#/room/#centos-integration:fedora.im>

* Bring topics to the Discourse forum:

    Use `centos-integration-sig` tag in the [Our Neighbors/CentOS](https://discussion.fedoraproject.org/c/neighbors/centos/71) category of the Fedora Discourse site.

* File Issues on Gitlab:

	<https://gitlab.com/CentOS/Integration/general/-/issues>

* Contribute docs on Gitlab:

	<https://gitlab.com/CentOS/Integration/docs>


# Questions

1. Is the scope limited to integrations done by CentOS SIGs?

	No. There are many ways one can integrate with the CentOS
Stream. CentOS Project provides the option to do it via a CentOS SIG,
but it is just one of the many possibilities. One can run integration
workflows on their own infrastructure (both in the open or in
private). We welcome everyone and would like to share the knowledge
and tooling (and work items :) ) as much as possible.

2. Is the scope limited to integration with other FOSS projects?

	No. While all the contributions (tooling, configurations, docs) of
the SIG should be open and licensed under the licenses approved by the
CentOS Project, you can participate in the SIG even when your primary
project/product or service is not public. For example you can use
integration tooling in your private labs and optionally contribute
test results without sharing the access to your test systems.
