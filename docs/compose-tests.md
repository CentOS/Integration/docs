Thanks to the work done by [Carlos
Rodriguez-Fernandez](https://accounts.centos.org/user/carlosrodrifernandez/)
the compose tests have been ported from the
[t_functional](https://git.centos.org/centos/t_functional) test
wrapper to the [tmt (Test Management Tool)](https://tmt.readthedocs.io/en/stable/overview.html).

You can find new tests repository in the Integration SIG namespace on Gitlab.com:

* <https://gitlab.com/CentOS/Integration/compose-tests>


# Where to see the tests in action

For CentOS Stream 9 these tests run in the experimental Jenkins job:

* <https://testing.stream.centos.org/job/CentOS_9-stream_x86_64_kvm_Experimental%20-%20TMT%20test%20suite/>

For CentOS Stream 10 we run the tests in the main Jenkins pipeline:

* <https://testing.stream.centos.org/job/CentOS_10-stream_all/>

# How to run the compose tests locally

1. Install the tmt tool and tmt plugin with libvirt support:

		# dnf install tmt tmt+provision-virtual

	See other installation methods in the [tmt
	documentation](https://tmt.readthedocs.io/en/stable/overview.html#install)

1. Checkout the git repository with tests:

		$ git clone https://gitlab.com/CentOS/Integration/compose-tests
		$ cd compose-tests/

1. Run the tests execution with a provision plugin of choice (see
   [provision](https://tmt.readthedocs.io/en/stable/plugins/provision.html)).

	!!! warning
		Compose tests are destructive and designed to be run
		on a test environment as root. Do not run them locally on a
		development workstation.

	Here, for example, we run tmt tests on a latest CentOS Stream 9 cloud image using `virtual` provision plugin:

		$ tmt -vvv -c distro=centos-stream-9 run --all \
			provision --how=virtual --image=centos-stream-9 \
			prepare --how=install --package=epel-release

	As some of the tests require test frameworks and libraries not
    available in the CentOS Stream repositories, we additionally
    enable EPEL repositories.
	
tmt tool has many useful features: test discovery, filtering tests by
labels, partial execution of test steps, easy re-run, management of
artifacts,.. Please refer to the official documentation and check the
[examples](https://tmt.readthedocs.io/en/stable/examples.html).

# How to debug a specific test failure

To reproduce the test locally in a virtual machine, use:

	$ tmt -vvv -c distro=centos-stream-9 \
		run --all plans --name ng tests --name /tests/curl \
		provision --how=virtual --image=centos-stream-9 \
		prepare --how=install --package=epel-release \
		login

Here we

1. run a chosen test (`/tests/curl` from `/plans/ng/`),
2. using the `virtual` provision plugin with the latest CentOS Stream
   9 cloud image,
3. while making EPEL enabled on the system before running the test.
4. We also login to the machine after the test is finished.

Running this command will execute the test and present the shell
inside the machine where you can check the artifacts produces by the
test run and perform a live debugging.

# How to contribute

New tests and test fixes are always welcome.

You can send merge requests to the test repository on Gitlab. Check
the
[README.md](https://gitlab.com/CentOS/Integration/compose-tests/-/blob/main/Readme.md)
for more information on the repository layout and validation.
