help:
	@echo "Run 'make test' to test locally or 'make build' to render the static site"

build:
	podman run --rm -it -v ${PWD}:/docs:z docker.io/squidfunk/mkdocs-material build

clean:
	rm -rf site

test:
	podman run --rm -it -p 8000:8000 -v ${PWD}:/docs:z docker.io/squidfunk/mkdocs-material

.PHONY: help build test
